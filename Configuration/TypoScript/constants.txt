
plugin.tx_psprojects_projectsteaser {
	view {
		# cat=plugin.tx_psprojects_projectsteaser/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:ps_projects/Resources/Private/Templates/
		# cat=plugin.tx_psprojects_projectsteaser/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:ps_projects/Resources/Private/Partials/
		# cat=plugin.tx_psprojects_projectsteaser/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:ps_projects/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_psprojects_projectsteaser//a; type=string; label=Default storage PID
		storagePid =
	}
}

plugin.tx_psprojects_projectlist {
	view {
		# cat=plugin.tx_psprojects_projectlist/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:ps_projects/Resources/Private/Templates/
		# cat=plugin.tx_psprojects_projectlist/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:ps_projects/Resources/Private/Partials/
		# cat=plugin.tx_psprojects_projectlist/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:ps_projects/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_psprojects_projectlist//a; type=string; label=Default storage PID
		storagePid =
	}
}

plugin.tx_psprojects_commentslist {
	view {
		# cat=plugin.tx_psprojects_commentslist/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:ps_projects/Resources/Private/Templates/
		# cat=plugin.tx_psprojects_commentslist/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:ps_projects/Resources/Private/Partials/
		# cat=plugin.tx_psprojects_commentslist/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:ps_projects/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_psprojects_commentslist//a; type=string; label=Default storage PID
		storagePid =
	}
}
