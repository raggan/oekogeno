
plugin.tx_psprojects_projectsteaser {
	view {
		templateRootPaths.0 = {$plugin.tx_psprojects_projectsteaser.view.templateRootPath}
		partialRootPaths.0 = {$plugin.tx_psprojects_projectsteaser.view.partialRootPath}
		layoutRootPaths.0 = {$plugin.tx_psprojects_projectsteaser.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_psprojects_projectsteaser.persistence.storagePid}
	}
}

plugin.tx_psprojects_projectlist {
	view {
		templateRootPaths.0 = {$plugin.tx_psprojects_projectlist.view.templateRootPath}
		partialRootPaths.0 = {$plugin.tx_psprojects_projectlist.view.partialRootPath}
		layoutRootPaths.0 = {$plugin.tx_psprojects_projectlist.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_psprojects_projectlist.persistence.storagePid}
	}
}

plugin.tx_psprojects_commentslist {
	view {
		templateRootPaths.0 = {$plugin.tx_psprojects_commentslist.view.templateRootPath}
		partialRootPaths.0 = {$plugin.tx_psprojects_commentslist.view.partialRootPath}
		layoutRootPaths.0 = {$plugin.tx_psprojects_commentslist.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_psprojects_commentslist.persistence.storagePid}
	}
}

plugin.tx_psprojects._CSS_DEFAULT_STYLE (
	textarea.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	input.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	.tx-ps-projects table {
		border-collapse:separate;
		border-spacing:10px;
	}

	.tx-ps-projects table th {
		font-weight:bold;
	}

	.tx-ps-projects table td {
		vertical-align:top;
	}

	.typo3-messages .message-error {
		color:red;
	}

	.typo3-messages .message-ok {
		color:green;
	}

)
