<?php
namespace PS\PsProjects\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Project
 */
class Project extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     */
    protected $title = '';
    
    /**
     * flagheadline
     *
     * @var string
     */
    protected $flagheadline = '';
    
    /**
     * flagtext
     *
     * @var string
     */
    protected $flagtext = '';
    
    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image = null;
    
    /**
     * text
     *
     * @var string
     */
    protected $text = '';
    
    /**
     * table1title
     *
     * @var string
     */
    protected $table1title = '';
    
    /**
     * table2title
     *
     * @var string
     */
    protected $table2title = '';
    
    /**
     * filestitle
     *
     * @var string
     */
    protected $filestitle = '';
    
    /**
     * filestext
     *
     * @var string
     */
    protected $filestext = '';
    
    /**
     * files
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $files = null;
    
    /**
     * gallerytitle
     *
     * @var string
     */
    protected $gallerytitle = '';
    
    /**
     * galleryimages
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $galleryimages = null;
    
    /**
     * teasertext
     *
     * @var string
     */
    protected $teasertext = '';
    
    /**
     * category
     *
     * @var \PS\PsProjects\Domain\Model\Category
     */
    protected $category = null;
    
    /**
     * infotables1
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PS\PsProjects\Domain\Model\InfoTable>
     * @cascade remove
     */
    protected $infotables1 = null;
    
    /**
     * infotables2
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PS\PsProjects\Domain\Model\InfoTable>
     * @cascade remove
     */
    protected $infotables2 = null;
    
    /**
     * comments
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PS\PsProjects\Domain\Model\Comment>
     * @cascade remove
     */
    protected $comments = null;
    
    /**
     * person
     *
     * @var \PS\PsProjects\Domain\Model\Person
     */
    protected $person = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->infotables1 = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->infotables2 = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->comments = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Returns the flagheadline
     *
     * @return string $flagheadline
     */
    public function getFlagheadline()
    {
        return $this->flagheadline;
    }
    
    /**
     * Sets the flagheadline
     *
     * @param string $flagheadline
     * @return void
     */
    public function setFlagheadline($flagheadline)
    {
        $this->flagheadline = $flagheadline;
    }
    
    /**
     * Returns the flagtext
     *
     * @return string $flagtext
     */
    public function getFlagtext()
    {
        return $this->flagtext;
    }
    
    /**
     * Sets the flagtext
     *
     * @param string $flagtext
     * @return void
     */
    public function setFlagtext($flagtext)
    {
        $this->flagtext = $flagtext;
    }
    
    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }
    
    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }
    
    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }
    
    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }
    
    /**
     * Returns the table1title
     *
     * @return string $table1title
     */
    public function getTable1title()
    {
        return $this->table1title;
    }
    
    /**
     * Sets the table1title
     *
     * @param string $table1title
     * @return void
     */
    public function setTable1title($table1title)
    {
        $this->table1title = $table1title;
    }
    
    /**
     * Returns the table2title
     *
     * @return string $table2title
     */
    public function getTable2title()
    {
        return $this->table2title;
    }
    
    /**
     * Sets the table2title
     *
     * @param string $table2title
     * @return void
     */
    public function setTable2title($table2title)
    {
        $this->table2title = $table2title;
    }
    
    /**
     * Returns the filestitle
     *
     * @return string $filestitle
     */
    public function getFilestitle()
    {
        return $this->filestitle;
    }
    
    /**
     * Sets the filestitle
     *
     * @param string $filestitle
     * @return void
     */
    public function setFilestitle($filestitle)
    {
        $this->filestitle = $filestitle;
    }
    
    /**
     * Returns the filestext
     *
     * @return string $filestext
     */
    public function getFilestext()
    {
        return $this->filestext;
    }
    
    /**
     * Sets the filestext
     *
     * @param string $filestext
     * @return void
     */
    public function setFilestext($filestext)
    {
        $this->filestext = $filestext;
    }
    
    /**
     * Returns the files
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $files
     */
    public function getFiles()
    {
        return $this->files;
    }
    
    /**
     * Sets the files
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $files
     * @return void
     */
    public function setFiles(\TYPO3\CMS\Extbase\Domain\Model\FileReference $files)
    {
        $this->files = $files;
    }
    
    /**
     * Returns the gallerytitle
     *
     * @return string $gallerytitle
     */
    public function getGallerytitle()
    {
        return $this->gallerytitle;
    }
    
    /**
     * Sets the gallerytitle
     *
     * @param string $gallerytitle
     * @return void
     */
    public function setGallerytitle($gallerytitle)
    {
        $this->gallerytitle = $gallerytitle;
    }
    
    /**
     * Returns the galleryimages
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $galleryimages
     */
    public function getGalleryimages()
    {
        return $this->galleryimages;
    }
    
    /**
     * Sets the galleryimages
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $galleryimages
     * @return void
     */
    public function setGalleryimages(\TYPO3\CMS\Extbase\Domain\Model\FileReference $galleryimages)
    {
        $this->galleryimages = $galleryimages;
    }
    
    /**
     * Returns the teasertext
     *
     * @return string $teasertext
     */
    public function getTeasertext()
    {
        return $this->teasertext;
    }
    
    /**
     * Sets the teasertext
     *
     * @param string $teasertext
     * @return void
     */
    public function setTeasertext($teasertext)
    {
        $this->teasertext = $teasertext;
    }
    
    /**
     * Returns the category
     *
     * @return \PS\PsProjects\Domain\Model\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Sets the category
     *
     * @param \PS\PsProjects\Domain\Model\Category $category
     * @return void
     */
    public function setCategory(\PS\PsProjects\Domain\Model\Category $category)
    {
        $this->category = $category;
    }
    
    /**
     * Adds a InfoTable
     *
     * @param \PS\PsProjects\Domain\Model\InfoTable $infotables1
     * @return void
     */
    public function addInfotables1(\PS\PsProjects\Domain\Model\InfoTable $infotables1)
    {
        $this->infotables1->attach($infotables1);
    }
    
    /**
     * Removes a InfoTable
     *
     * @param \PS\PsProjects\Domain\Model\InfoTable $infotables1ToRemove The InfoTable to be removed
     * @return void
     */
    public function removeInfotables1(\PS\PsProjects\Domain\Model\InfoTable $infotables1ToRemove)
    {
        $this->infotables1->detach($infotables1ToRemove);
    }
    
    /**
     * Returns the infotables1
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PS\PsProjects\Domain\Model\InfoTable> $infotables1
     */
    public function getInfotables1()
    {
        return $this->infotables1;
    }
    
    /**
     * Sets the infotables1
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PS\PsProjects\Domain\Model\InfoTable> $infotables1
     * @return void
     */
    public function setInfotables1(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $infotables1)
    {
        $this->infotables1 = $infotables1;
    }
    
    /**
     * Adds a InfoTable
     *
     * @param \PS\PsProjects\Domain\Model\InfoTable $infotables2
     * @return void
     */
    public function addInfotables2(\PS\PsProjects\Domain\Model\InfoTable $infotables2)
    {
        $this->infotables2->attach($infotables2);
    }
    
    /**
     * Removes a InfoTable
     *
     * @param \PS\PsProjects\Domain\Model\InfoTable $infotables2ToRemove The InfoTable to be removed
     * @return void
     */
    public function removeInfotables2(\PS\PsProjects\Domain\Model\InfoTable $infotables2ToRemove)
    {
        $this->infotables2->detach($infotables2ToRemove);
    }
    
    /**
     * Returns the infotables2
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PS\PsProjects\Domain\Model\InfoTable> $infotables2
     */
    public function getInfotables2()
    {
        return $this->infotables2;
    }
    
    /**
     * Sets the infotables2
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PS\PsProjects\Domain\Model\InfoTable> $infotables2
     * @return void
     */
    public function setInfotables2(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $infotables2)
    {
        $this->infotables2 = $infotables2;
    }
    
    /**
     * Adds a Comment
     *
     * @param \PS\PsProjects\Domain\Model\Comment $comment
     * @return void
     */
    public function addComment(\PS\PsProjects\Domain\Model\Comment $comment)
    {
        $this->comments->attach($comment);
    }
    
    /**
     * Removes a Comment
     *
     * @param \PS\PsProjects\Domain\Model\Comment $commentToRemove The Comment to be removed
     * @return void
     */
    public function removeComment(\PS\PsProjects\Domain\Model\Comment $commentToRemove)
    {
        $this->comments->detach($commentToRemove);
    }
    
    /**
     * Returns the comments
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PS\PsProjects\Domain\Model\Comment> $comments
     */
    public function getComments()
    {
        return $this->comments;
    }
    
    /**
     * Sets the comments
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\PS\PsProjects\Domain\Model\Comment> $comments
     * @return void
     */
    public function setComments(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $comments)
    {
        $this->comments = $comments;
    }
    
    /**
     * Returns the person
     *
     * @return \PS\PsProjects\Domain\Model\Person $person
     */
    public function getPerson()
    {
        return $this->person;
    }
    
    /**
     * Sets the person
     *
     * @param \PS\PsProjects\Domain\Model\Person $person
     * @return void
     */
    public function setPerson(\PS\PsProjects\Domain\Model\Person $person)
    {
        $this->person = $person;
    }

}