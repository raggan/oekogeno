<?php

namespace PS\PsProjects\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \PS\PsProjects\Domain\Model\Project.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 */
class ProjectTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \PS\PsProjects\Domain\Model\Project
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \PS\PsProjects\Domain\Model\Project();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFlagheadlineReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getFlagheadline()
		);
	}

	/**
	 * @test
	 */
	public function setFlagheadlineForStringSetsFlagheadline()
	{
		$this->subject->setFlagheadline('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'flagheadline',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFlagtextReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getFlagtext()
		);
	}

	/**
	 * @test
	 */
	public function setFlagtextForStringSetsFlagtext()
	{
		$this->subject->setFlagtext('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'flagtext',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImageReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getImage()
		);
	}

	/**
	 * @test
	 */
	public function setImageForFileReferenceSetsImage()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImage($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'image',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTextReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getText()
		);
	}

	/**
	 * @test
	 */
	public function setTextForStringSetsText()
	{
		$this->subject->setText('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'text',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTable1titleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTable1title()
		);
	}

	/**
	 * @test
	 */
	public function setTable1titleForStringSetsTable1title()
	{
		$this->subject->setTable1title('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'table1title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTable2titleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTable2title()
		);
	}

	/**
	 * @test
	 */
	public function setTable2titleForStringSetsTable2title()
	{
		$this->subject->setTable2title('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'table2title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFilestitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getFilestitle()
		);
	}

	/**
	 * @test
	 */
	public function setFilestitleForStringSetsFilestitle()
	{
		$this->subject->setFilestitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'filestitle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFilestextReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getFilestext()
		);
	}

	/**
	 * @test
	 */
	public function setFilestextForStringSetsFilestext()
	{
		$this->subject->setFilestext('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'filestext',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFilesReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getFiles()
		);
	}

	/**
	 * @test
	 */
	public function setFilesForFileReferenceSetsFiles()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setFiles($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'files',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getGallerytitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getGallerytitle()
		);
	}

	/**
	 * @test
	 */
	public function setGallerytitleForStringSetsGallerytitle()
	{
		$this->subject->setGallerytitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'gallerytitle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getGalleryimagesReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getGalleryimages()
		);
	}

	/**
	 * @test
	 */
	public function setGalleryimagesForFileReferenceSetsGalleryimages()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setGalleryimages($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'galleryimages',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTeasertextReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTeasertext()
		);
	}

	/**
	 * @test
	 */
	public function setTeasertextForStringSetsTeasertext()
	{
		$this->subject->setTeasertext('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'teasertext',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCategoryReturnsInitialValueForCategory()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getCategory()
		);
	}

	/**
	 * @test
	 */
	public function setCategoryForCategorySetsCategory()
	{
		$categoryFixture = new \PS\PsProjects\Domain\Model\Category();
		$this->subject->setCategory($categoryFixture);

		$this->assertAttributeEquals(
			$categoryFixture,
			'category',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getInfotables1ReturnsInitialValueForInfoTable()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getInfotables1()
		);
	}

	/**
	 * @test
	 */
	public function setInfotables1ForObjectStorageContainingInfoTableSetsInfotables1()
	{
		$infotables1 = new \PS\PsProjects\Domain\Model\InfoTable();
		$objectStorageHoldingExactlyOneInfotables1 = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneInfotables1->attach($infotables1);
		$this->subject->setInfotables1($objectStorageHoldingExactlyOneInfotables1);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneInfotables1,
			'infotables1',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addInfotables1ToObjectStorageHoldingInfotables1()
	{
		$infotables1 = new \PS\PsProjects\Domain\Model\InfoTable();
		$infotables1ObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$infotables1ObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($infotables1));
		$this->inject($this->subject, 'infotables1', $infotables1ObjectStorageMock);

		$this->subject->addInfotables1($infotables1);
	}

	/**
	 * @test
	 */
	public function removeInfotables1FromObjectStorageHoldingInfotables1()
	{
		$infotables1 = new \PS\PsProjects\Domain\Model\InfoTable();
		$infotables1ObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$infotables1ObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($infotables1));
		$this->inject($this->subject, 'infotables1', $infotables1ObjectStorageMock);

		$this->subject->removeInfotables1($infotables1);

	}

	/**
	 * @test
	 */
	public function getInfotables2ReturnsInitialValueForInfoTable()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getInfotables2()
		);
	}

	/**
	 * @test
	 */
	public function setInfotables2ForObjectStorageContainingInfoTableSetsInfotables2()
	{
		$infotables2 = new \PS\PsProjects\Domain\Model\InfoTable();
		$objectStorageHoldingExactlyOneInfotables2 = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneInfotables2->attach($infotables2);
		$this->subject->setInfotables2($objectStorageHoldingExactlyOneInfotables2);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneInfotables2,
			'infotables2',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addInfotables2ToObjectStorageHoldingInfotables2()
	{
		$infotables2 = new \PS\PsProjects\Domain\Model\InfoTable();
		$infotables2ObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$infotables2ObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($infotables2));
		$this->inject($this->subject, 'infotables2', $infotables2ObjectStorageMock);

		$this->subject->addInfotables2($infotables2);
	}

	/**
	 * @test
	 */
	public function removeInfotables2FromObjectStorageHoldingInfotables2()
	{
		$infotables2 = new \PS\PsProjects\Domain\Model\InfoTable();
		$infotables2ObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$infotables2ObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($infotables2));
		$this->inject($this->subject, 'infotables2', $infotables2ObjectStorageMock);

		$this->subject->removeInfotables2($infotables2);

	}

	/**
	 * @test
	 */
	public function getCommentsReturnsInitialValueForComment()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getComments()
		);
	}

	/**
	 * @test
	 */
	public function setCommentsForObjectStorageContainingCommentSetsComments()
	{
		$comment = new \PS\PsProjects\Domain\Model\Comment();
		$objectStorageHoldingExactlyOneComments = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneComments->attach($comment);
		$this->subject->setComments($objectStorageHoldingExactlyOneComments);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneComments,
			'comments',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addCommentToObjectStorageHoldingComments()
	{
		$comment = new \PS\PsProjects\Domain\Model\Comment();
		$commentsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$commentsObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($comment));
		$this->inject($this->subject, 'comments', $commentsObjectStorageMock);

		$this->subject->addComment($comment);
	}

	/**
	 * @test
	 */
	public function removeCommentFromObjectStorageHoldingComments()
	{
		$comment = new \PS\PsProjects\Domain\Model\Comment();
		$commentsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$commentsObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($comment));
		$this->inject($this->subject, 'comments', $commentsObjectStorageMock);

		$this->subject->removeComment($comment);

	}

	/**
	 * @test
	 */
	public function getPersonReturnsInitialValueForPerson()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getPerson()
		);
	}

	/**
	 * @test
	 */
	public function setPersonForPersonSetsPerson()
	{
		$personFixture = new \PS\PsProjects\Domain\Model\Person();
		$this->subject->setPerson($personFixture);

		$this->assertAttributeEquals(
			$personFixture,
			'person',
			$this->subject
		);
	}
}
