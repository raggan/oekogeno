<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'PS.' . $_EXTKEY,
	'Projectsteaser',
	'Projects Teaser'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'PS.' . $_EXTKEY,
	'Projectlist',
	'Projects List'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'PS.' . $_EXTKEY,
	'Commentslist',
	'Comments List'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Ps Projects');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_psprojects_domain_model_project', 'EXT:ps_projects/Resources/Private/Language/locallang_csh_tx_psprojects_domain_model_project.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_psprojects_domain_model_project');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_psprojects_domain_model_category', 'EXT:ps_projects/Resources/Private/Language/locallang_csh_tx_psprojects_domain_model_category.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_psprojects_domain_model_category');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_psprojects_domain_model_infotable', 'EXT:ps_projects/Resources/Private/Language/locallang_csh_tx_psprojects_domain_model_infotable.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_psprojects_domain_model_infotable');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_psprojects_domain_model_comment', 'EXT:ps_projects/Resources/Private/Language/locallang_csh_tx_psprojects_domain_model_comment.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_psprojects_domain_model_comment');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_psprojects_domain_model_person', 'EXT:ps_projects/Resources/Private/Language/locallang_csh_tx_psprojects_domain_model_person.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_psprojects_domain_model_person');
