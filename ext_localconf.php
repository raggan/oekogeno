<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'PS.' . $_EXTKEY,
	'Projectsteaser',
	array(
		'Project' => 'list, show',
		'Category' => 'list, show',
		'InfoTable' => 'list, show',
		'Comment' => 'list, show',
		'Person' => 'list, show',
		
	),
	// non-cacheable actions
	array(
		'Project' => '',
		'Category' => '',
		'InfoTable' => '',
		'Comment' => '',
		'Person' => '',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'PS.' . $_EXTKEY,
	'Projectlist',
	array(
		'Project' => 'list, show',
		'Category' => 'list, show',
		'InfoTable' => 'list, show',
		'Comment' => 'list, show',
		'Person' => 'list, show',
		
	),
	// non-cacheable actions
	array(
		'Project' => '',
		'Category' => '',
		'InfoTable' => '',
		'Comment' => '',
		'Person' => '',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'PS.' . $_EXTKEY,
	'Commentslist',
	array(
		'Comment' => 'list, show',
		
	),
	// non-cacheable actions
	array(
		'Project' => '',
		'Category' => '',
		'InfoTable' => '',
		'Comment' => '',
		'Person' => '',
		
	)
);
